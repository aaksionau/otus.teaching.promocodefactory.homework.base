﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Dto;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<EmployeeShortResponse> CreateEmployeeAsync(EmployeeDto model)
        {
            Employee entity = MapModel(model);
            var employee = await _employeeRepository.AddEntityAsync(entity);
            return new EmployeeShortResponse()
            {
                FullName = $"{employee.FullName}",
                Email = employee.Email,
                Id = employee.Id
            };

        }
        /// <summary>Удалить сотрудника</summary>
        /// <param name="employeeId">Id сотрудника</param>
        [HttpDelete("{employeeId}")]
        public async Task<IActionResult> RemoveEmployeeAsync(Guid employeeId)
        {
            await _employeeRepository.DeteleEntityAsync(employeeId);
            return NotFound();
        }

        private static Employee MapModel(EmployeeDto model)
        {
            return new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email
            };
        }
        /// <summary>Обновить данные сотрудника</summary>
        [HttpPut("{employeeId}")]
        public async Task<EmployeeResponse> UpdateEmployeeAsync(Guid employeeId, EmployeeDto model)
        {
            var entity = MapModel(model);
            await _employeeRepository.UpdateEntityAsync(employeeId, entity);
            return new EmployeeResponse()
            {
                FullName = $"{model.FirstName} {model.LastName}",
                Email = model.Email,
                Id = model.Id
            };
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
    }
}