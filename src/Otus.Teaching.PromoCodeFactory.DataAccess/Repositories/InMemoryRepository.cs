﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = new List<T>();
            Data.AddRange(data);
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> AddEntityAsync(T entity)
        {
            Data.Add(entity);
            return Task.FromResult(entity);
        }

        public Task<T> UpdateEntityAsync(Guid id, T entity)
        {
            var existingEntity = Data.FirstOrDefault(e=>e.Id == id);
            CopyProperties(entity, existingEntity);
            return Task.FromResult(existingEntity);
        }

        public Task DeteleEntityAsync(Guid id)
        {
            var existingEntity = Data.FirstOrDefault(e=>e.Id == id);
            Data.Remove(existingEntity);
            return Task.CompletedTask;
        }

        public void CopyProperties(object copyFrom, object copyTo){
            foreach (PropertyInfo property in typeof(T).GetProperties())
            {
                if (property.CanWrite && property.Name != "Id")
                {
                    property.SetValue(copyTo, property.GetValue(copyFrom, null), null);
                }
            }
        }
    }
}